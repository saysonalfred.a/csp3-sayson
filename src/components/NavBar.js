import React, {useState, useContext} from 'react';
import {NavLink, Link} from 'react-router-dom'

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import Swal from 'sweetalert2'

import UserContext from '../userContext'

import SignUpModal from './signupModal'
import LoginModal from './loginModal'

export default function NavBar(){

	const {user, setUser, unsetUser, handleShow, setHandleShow} = useContext(UserContext)

	const [signupmodalShow, setSignupModalShow] = useState(false);
	const [loginmodalShow, setLoginModalShow] = useState(false);

	function logout(){
		unsetUser()
		setUser({
			email: null,
			isAdmin: null
		})

		window.location.replace('/')

	}

	return (

		<Navbar bg="dark" variant="dark" expand="lg">
			<Navbar.Brand as={Link} to="/">
				<img alt="guitar" src="/images/NavBar/NavBrandIcon.png" width="30" height="30" className="d-inline-block align-top"/>{' '}Gitara</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">
					<Nav.Link as={NavLink} to="/">Home</Nav.Link>
					<Nav.Link as={NavLink} to="/products">Products</Nav.Link>
					{
						user.email
						? 
							user.isAdmin
							?
								<>
									<Nav.Link as={NavLink} to="/addProduct">Add Product</Nav.Link>
									<Nav.Link onClick={logout}>Logout</Nav.Link>
								</>
							:
								<Nav.Link onClick={logout}>Logout</Nav.Link>

						:
							<>
								<Nav.Link as={NavLink} onClick={() => setHandleShow(true)} to="/">Sign Up</Nav.Link>
									<SignUpModal
									        show={handleShow}
									        onHide={() => setHandleShow(false)}
									      />
								<Nav.Link as={NavLink} onClick={() => setLoginModalShow(true)} to="/">Login</Nav.Link>
									<LoginModal
									        show={loginmodalShow}
									        onHide={() => setLoginModalShow(false)}
									      />
							</>
					}
				</Nav>
			</Navbar.Collapse>
		</Navbar>

		)
}