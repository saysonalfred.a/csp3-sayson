import React, {useState} from 'react'
import {Container} from 'react-bootstrap'
import {BrowserRouter as Router} from 'react-router-dom'
import {Route, Switch} from 'react-router-dom'

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import NavBar from './components/NavBar'
import Home from './pages/home'
import Products from './pages/products'
import AddProduct from './pages/addProduct'
import NotFound from './pages/notFound'

import {UserProvider} from './userContext'

function App() {

  const[handleShow, setHandleShow] = useState(false)

  const [user, setUser] = useState({
    email: localStorage.getItem('email'),
    isAdmin: localStorage.getItem('isAdmin') === "true"
  })

  console.log(user)

  function unsetUser(){
    localStorage.clear()
  }

  return (
    <>
    <div>
      <UserProvider value={{user, setUser, unsetUser, handleShow, setHandleShow}}>
        <Router>
        <NavBar/>
          <Container fluid>
              <Switch>
                <Route exact path="/" component={Home}/>
                <Route exact path="/products" component={Products}/>
                <Route exact path="/addProduct" component={AddProduct}/>
                <Route component={NotFound}/>
              </Switch>
           </Container>
        </Router>
      </UserProvider> 
     </div> 
    </>
    )
}

export default App;
