import React from 'react'
import {Modal, Button} from 'react-bootstrap'
import SignUp from './SignUp'


export default function SignUpModal(signupModalProps) {
  return (
    <Modal
      {...signupModalProps}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Sign Up Form
        </Modal.Title>
      </Modal.Header>
      	<Modal.Body>
        	<SignUp/>
      	</Modal.Body>
      <Modal.Footer>
        <Button onClick={signupModalProps.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}