import React from 'react';
import {Carousel} from 'react-bootstrap'
import './Components.css'

// import UserContext from '../userContext'

export default function homeCarousel({}){
	return(
		<Carousel id="carousel">
		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src="/images/Carousel/img1.jpg"
		      alt="First slide"
		    />
		    <Carousel.Caption>
		      <h1 id="carouselCaption">Gretsch</h1>
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src="/images/Carousel/img2.jpg"
		      alt="Second slide"
		    />

		    <Carousel.Caption>
		      <h3>Fender</h3>
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src="/images/Carousel/img3.jpg"
		      alt="Third slide"
		    />

		    <Carousel.Caption>
		      <h3>Tagima</h3>
		    </Carousel.Caption>
		  </Carousel.Item>
		</Carousel>

		)
}