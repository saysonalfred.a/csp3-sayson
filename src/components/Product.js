import React, {useState, useEffect} from 'react'
import {Card, Button, Row, Col} from 'react-bootstrap'

export default function Product({productProp}){

	const [count, setCount] = useState(0)
	const [stock, setStock] = useState(30)
	const [isActive, setIsActive] = useState(true)

	useEffect(() => {
		if(stock === 0){
			setIsActive(false)
		}
	}, [stock])

	function addToCart(){
		setCount(count + 1)
		setStock(stock - 1)
	}


	return(

				<Card className="productCard">
					<Card.Body>
						<Card.Title>
							<h2>{productProp.productName}</h2>
						</Card.Title>
						<Card.Text>
						{productProp.productDesc}
						</Card.Text>
					</Card.Body>
					<Card.Footer>
					<Card.Text>
						Price: {productProp.productPrice}
					</Card.Text>
					<Card.Text>
						Stock Available: {
							stock <= 0 
							? <span className="text-danger">0</span>
							: <span className="text-success">{stock}</span>
						}
					</Card.Text>
					<Card.Text>
						Quantity: {
							count === 0 
							? <span className="text-danger">0</span>
							: <span className="text-success">{count}</span>
						}
					</Card.Text>
						{
							isActive === false
							? <Button variant="danger" disabled>Out of Stock</Button>
							: <Button variant="dark" onClick={addToCart}>Add to Cart</Button>
						}
					</Card.Footer>
				</Card>
		)
}