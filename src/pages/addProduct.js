import React, {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2'
import {Redirect} from 'react-router-dom'
import UserContext from '../userContext'

export default function AddProduct(){
	const[productName, setProductName] = useState("")
	const[productDesc, setProductDesc] = useState("")
	const[productPrice, setProductPrice] = useState(0)
	const[isActive, setIsActive] = useState(false)
	const {user} = useContext(UserContext)

	const[willRedirect, setWillRedirect] = useState(false)
	useEffect(() => {
		if(productName !== "" && productDesc !== "" && productPrice !== ""){
			setIsActive(true)
		} else {

			setIsActive(false)
		}
	}, [productName, productDesc, productPrice])

	function addAProduct(event){
		event.preventDefault()
		let token = localStorage.getItem('token')
		fetch('http://powerful-island-23840.herokuapp.com/api/addProduct', {

			method: "POST",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productName: productName,
				productDesc: productDesc,
				productPrice: productPrice
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)
			if(data.message){
				Swal.fire({
					icon: "error",
					title: "Adding Product Failed!",
					text: data.message
				})
			} else {
				Swal.fire({
					icon: "success",
					title: "New Product has been added!",
				})
				console.log(productName)
				console.log(productDesc)
				console.log(productPrice)
				setWillRedirect(true)

				setProductName("")
				setProductDesc("")
				setProductPrice(0)		
			}
		})

		
	}
	return(
		!user.isAdmin
		?
		<Redirect to='/'/>
		:
		<Form onSubmit = {event => addAProduct(event)}>
			<Form.Group>
				<Form.Label>Product Name:</Form.Label>
				<Form.Control type="text" placeholder="Enter Product Name" value={productName} onChange={ event => {
					setProductName(event.target.value)
				}}required/>

				<Form.Label>Product Description:</Form.Label>
				<Form.Control type="text" placeholder="Enter Product Description" value={productDesc} onChange={ event => {
					setProductDesc(event.target.value)
				}}required/>

				<Form.Label>Product Price:</Form.Label>
				<Form.Control type="text" placeholder="Enter Product Price" value={productPrice} onChange={ event => {
					setProductPrice(event.target.value)
				}}required/>
			</Form.Group>
			{
				isActive
			
				?<Button variant="dark" type="submit">Submit</Button>
				:<Button variant="danger" disabled>Submit</Button>
			}	
		</Form>

		)
}