import React from 'react'
import Banner from '../components/Banner'

export default function notFound(){

	let bannerContent = {
	    title: "Page not found",
	    label: "Home",
	    destination: "/"
	  }

	return(
		<>
			<Banner bannerProp={bannerContent}/>
		</>


		)
}