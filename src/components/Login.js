import React, {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2'

import UserContext from '../userContext'


export default function Login(){

	const {user, setUser, handleShow, setHandleShow} = useContext(UserContext)

	const[email, setEmail] = useState("")
	const[password, setPassword] = useState("")
	const[isActive, setIsActive] = useState(false)
	
	useEffect(() => {
		if(email !== "" && password !== ""){

			setIsActive(true)
		} else {

			setIsActive(false)
		}
	}, [email, password])

	

	function loginUser(event){
		event.preventDefault()
		fetch('https://powerful-island-23840.herokuapp.com/api/login', {

			method: "POST",
			headers:{
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => {
			if(data.message){
				Swal.fire({
					icon: "error",
					title: "Login Failed!",
					text: data.message
				})
			} else {

				localStorage.setItem('token', data.accessToken)
				fetch('https://powerful-island-23840.herokuapp.com/api/profile', { 
					headers: {
						Authorization:`Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					localStorage.setItem('email', data.email)
					localStorage.setItem('isAdmin', data.isAdmin)
					setUser({

						email: data.email,
						isAdmin: data.isAdmin

					})

					Swal.fire({
						icon: "success",
						title: `You are now logged in`
					})

					setHandleShow(false)

				})
			}
			setEmail("")
			setPassword("")
		})	
}
	return(
		<Form onSubmit = {event => loginUser(event)}>
			<Form.Group>
				<Form.Label>Email</Form.Label>
				<Form.Control type="text" placeholder="Enter Email" value={email} onChange={ event => {
					setEmail(event.target.value)
				}}required/>

				<Form.Label>Password:</Form.Label>
				<Form.Control type="password" placeholder="Enter Password" value={password} onChange={ event => {
					setPassword(event.target.value)
				}}required/>
			</Form.Group>
			{
				isActive
			
				?
				<Button variant="dark" type="submit">Login</Button>
				:
				<Button variant="dark" disabled>Login</Button>
			}	
		</Form>

		
		)
}

