import React from 'react'
import {Modal, Button} from 'react-bootstrap'
import Login from './Login'

export default function LoginModal(loginModalProps) {
  return (
    <Modal
      {...loginModalProps}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Log In
        </Modal.Title>
      </Modal.Header>
      	<Modal.Body>
        	<Login/>
      	</Modal.Body>
      <Modal.Footer>
        <Button onClick={loginModalProps.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}