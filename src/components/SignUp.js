import React, {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2'
import LoginModal from './loginModal'
import UserContext from '../userContext'


export default function Register(){
	const{handleShow, setHandleShow} = useContext(UserContext)
	const[firstName, setFirstName] = useState("")
	const[lastName, setLastName] = useState("")
	const[email, setEmail] = useState("")
	const[password, setPassword] = useState("")
	const[confirmPassword, setConfirmPassword] = useState("")

	const[isActive, setIsActive] = useState(false)
	const[isMatched, setIsMatched] = useState(false)
	console.log(isMatched)
	const [loginmodalShow, setLoginModalShow] = useState(false);
	
	useEffect(() => {
		if((firstName !== "" && lastName !== "" && email !== ""  && password !== "" && confirmPassword !== "") && (password === confirmPassword)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}

		if(password === "" && confirmPassword === ""){
			setIsMatched(true)
		} else if(password !== confirmPassword && password !== "" && confirmPassword !== ""){
			setIsMatched(false)

		} else {
			setIsMatched(true)
		}



	}, [firstName, lastName, email, password, confirmPassword])

	function registerUser(event){
		event.preventDefault()

		fetch('https://powerful-island-23840.herokuapp.com/api/register', {

			method: "POST",
			headers:{
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(event)
			console.log(data)
			if(data.message){
				Swal.fire({
					icon: "error",
					title: "Registration Failed.",
					text: data.message
				})
			} else {
				Swal.fire({
					icon: "success",
					title: "Registration Successful!",
					text: "You may now log in."
				})

				setLoginModalShow(true);
				

				setFirstName("")
				setLastName("")
				setEmail("")
				setPassword("")
				setConfirmPassword("")
			}
		})

		
	} 

	return(
		
		
		loginmodalShow
		?
		<LoginModal show={loginmodalShow} onHide={() => setLoginModalShow(false)}/>
		:
		<Form onSubmit = {event => registerUser(event)}>
			<Form.Group>
				<Form.Label>First Name:</Form.Label>
				<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={ event => {
					setFirstName(event.target.value)
				}}required/>

				<Form.Label>Last Name:</Form.Label>
				<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={ event => {
					setLastName(event.target.value)
				}}required/>

				<Form.Label>Email</Form.Label>
				<Form.Control type="text" placeholder="Enter Email" value={email} onChange={ event => {
					setEmail(event.target.value)
				}}required/>

				<Form.Label>Password:</Form.Label>
				<Form.Control type="password" placeholder="Enter Password" value={password} onChange={ event => {
					setPassword(event.target.value)
				}}required/>

				<Form.Label>Confirm Password:</Form.Label>
				<Form.Control type="password" placeholder="Confirm Password" value={confirmPassword} onChange={ event => {
					setConfirmPassword(event.target.value)
				}}required/>
				{
					isMatched
					?
					<></>
					:
					<span className="text-danger">Password does not match.</span>
				}

			</Form.Group>
			{
				isActive
			
				?<Button variant="primary" type="submit">Submit</Button>
				:<Button variant="primary" disabled>Submit</Button>
			}	
		</Form>

		

		
		)


}