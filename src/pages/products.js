import React, {useState, useEffect, useContext} from 'react'
import {Table, Button, Row, Col} from 'react-bootstrap'
import Banner from '../components/Banner'
import Product from '../components/Product'
import UserContext from '../userContext'

export default function Products(){
	const {user} = useContext(UserContext)
	const[allProducts, setAllProducts] = useState([]);
	const[activeProducts, setActiveProducts] = useState([]);
	const[update, setUpdate] = useState("")

	useEffect(() => {
		fetch('https://powerful-island-23840.herokuapp.com/api/allproducts', {
			headers:{
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			} 
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
				setAllProducts(data.result)

				let productsTemp = data.result
				let tempArray = productsTemp.filter(product => {
					return product.isActive === true
				})
				setActiveProducts(tempArray)
			})

		},[update])

	console.log(activeProducts)

	let productComponents = activeProducts.map(product => {
		console.log(product)
	    return (
	    		
	    		<Col lg="3" md="3" sm="6" className="pt-3 pb-3">
	    			<Product key={product._id} productProp={product}/>
	    		</Col>
	    	)
	})

	console.log(productComponents)


	
	
	function archive(productId){

		fetch(`https://powerful-island-23840.herokuapp.com/api/products/archive/${productId}`,{

			method: 'PUT',
			headers:{
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			} 
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setUpdate({})
		})
	}

	function activate(productId){
		fetch(`https://powerful-island-23840.herokuapp.com/api/products/activate/${productId}`,{

			method: 'PUT',
			headers:{
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			} 
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setUpdate({})
		})	

	}

	console.log(allProducts)
	let productRows = allProducts.map(product => {
		return(

				<tr key={product._id}>
					<td>{product._id}</td>
					<td>{product.productName}</td>
					<td className={product.isActive 
						? "text-success" 
						: "text-danger"}>
						{
							product.isActive
							?
							"Active"
							:
							"Inactive"
						}
					</td>
					<td>
						{
							product.isActive
							?
							<Button variant="danger" className="mx-2" 
							onClick={ () => archive(product._id)}>Archive</Button>
							:
							<Button variant="success" className="mx-2" 
							onClick={ () => activate(product._id)}>Activate</Button>
						}
					</td>	
				</tr>

			)
	})

	console.log(productRows)

	let bannerContent = {
	    title: "Products",
	    label: "Home",
	    destination: "/"
	  }

	return(
		user.isAdmin === true
		?
		<>
			<h1 className="text-center">Admin Dashboard</h1>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Status</th>
						<th>Actions</th>

					</tr>
				</thead>
				<tbody>
					{productRows}
				</tbody>
			</Table>
		</>
		:
		<>
			<Banner bannerProp={bannerContent}/>
			<Row>
				{productComponents}
			</Row>
		</>
		)
}